from django.db import models
from .import validators

# Create your models here.
class Company(models.Model):
    name=models.CharField(max_length=50,help_text="Max length 50")
    logo=models.ImageField(null=True)
    bussiness_type=models.CharField(max_length=20,help_text="Max length 20")
    city=models.CharField(max_length=30,help_text="Max length 30")
    domain=models.URLField(max_length=200,help_text="Max length 200")
    top_general=models.PositiveSmallIntegerField()
    rating=models.FloatField()
    exclude =('top_general','rasting')

    def __str__(self):
        return self.name

class Complaint_Category(models.Model):
    name=models.CharField(max_length=50,help_text="Max length 50")

    def __str__(self):
        return self.name


class Complaint(models.Model):
    text_complaint=models.TextField()
    company = models.ForeignKey(Company, related_name='company')
    categories=models.ManyToManyField(Complaint_Category)
    created_time = models.DateTimeField('Created Time', auto_now_add=True,null=True)
    numbers=models.IntegerField(default=1,null=True)

    def __str__(self):
        return (str(self.numbers))



