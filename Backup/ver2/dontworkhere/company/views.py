from django.shortcuts import render
from django.template.loader import get_template
from django.template import Context
from django.http import HttpResponse,Http404
import datetime
# Create your views here.

def index(request):
    now=datetime.datetime.now()
    t = get_template('main.html')
    return render(request,'main.html',{'current_date':now})

    #
    # html=t.render(Context({'current_date':now}))
    # return HttpResponse(html)
